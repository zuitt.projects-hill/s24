// Query OPerators and Field Projection
	db.newProducts.insertMany([
		{
			"name" : "Iphone X",
			"price" : 30000,
			"isActive" : true
		},
		{
			"name" : "Samsung Galaxy S21",
			"price" : 51000,
			"isActive" : true
		},
		{
			"name" : "Razer Blackshark V2X",
			"price" : 2800,
			"isActive" : false
		},
		{
			"name" : "RAKK Gaming Mouse",
			"price" : 1800,
			"isActive" : true
		},
		{
			"name" : "Razer Mechanical Keyboard",
			"price" : 4000,
			"isActive" : true
		},

		
	])

// Query Operators
	// allow for more flexible querying in MongoDB
	// Instead of just being able to find/search for documents with exact values
	// We can use query operators define conditions instead of just specific criteria

// $gt, $lt, $gte and $lte - numbers
	
	// $gt - greater than

	db.newProducts.find({"price" : {$gt: 3000}})

	// $gte - greater than or equal

	db.newProducts.find({"price" : {$gte: 30000}})

	// $lt - less than

	db.newProducts.find({"price" : {$lt: 4000}})

	// $lte - less than or equal

	db.newProducts.find({"price" : {$lte: 2800}})

// note- Query operators can be used to expand the search criteria for updating and deleting

	db.newProducts.updateMany(
		{"price" : {$gte: 30000}
		}, 
		{$set: {"isActive" : false}
		}
	)

db.users.insertMany([
		{
			"firstName" : "Mary Jane",
			"lastName" : "Watson",
			"email" : "mjtiger@gmail.com",
			"password" : "tigerjackpot15",
			"isAdmin" : false
		},
		{
			"firstName" : "Gwen",
			"lastName" : "Stacy",
			"email" : "stacyTech@gmail.com",
			"password" : "stacyTech1991",
			"isAdmin" : true
		},
		{
			"firstName" : "Peter",
			"lastName" : "Parker",
			"email" : "peterWebDev@gmail.com",
			"password" : "webDeveloperPeter",
			"isAdmin" : true
		},
		{
			"firstName" : "Jonah",
			"lastName" : "Jameson",
			"email" : "JJJameson@gmail.com",
			"password" : "spideyisamenace",
			"isAdmin" : false
		},
		{
			"firstName" : "Otto",
			"lastName" : "Octavious",
			"email" : "ottoOctopi@gmail.com",
			"password" : "docOck15",
			"isAdmin" : true
		},
	])

// $regex - this query operator will allow us to find documents in which it will match the characters/ pattern of the character we are looking for

// Note: $regex is case sensitive

	db.users.find(
		{
			"firstName" : {$regex: "O"
		}
	});

	db.users.find(
		{
			"firstName" : {$regex: "W"
		}
	});

// $options - `$i` - for case sensitivity
	db.users.find(
		{
			"lastName" : {$regex: "o", $options: `$i`
		}
	});

// find documents that matches a specific word
	
	db.users.find({
		"email" : {$regex: 'web', $options: `$i`}
	})

	db.newProducts.find({
		"name" : {$regex: 'phone', $options: `$i`}
	})



	db.newProducts.find({
		"name" : {$regex: 'razer', $options: `$i`}
	})
	db.newProducts.find({
		"name" : {$regex: 'rakk', $options: `$i`}
	})

// $or and $and

	// $or operator - allows us to have a logical operator wherein we can look or find for a document which can satisfy at least one of our condtions.

	db.newProducts.find({
		$or: [
		{
			"name" : {$regex: 'x', $options: `$i`}
		},
		{
			"price" : {$lte: 10000}
		}
	]
})

	db.newProducts.find({
		$or: [
		{
			"name" : {$regex: 'x', $options: `$i`}
		},
		{
			"price" : 30000
		}
	]
})

	db.users.find({
		$or: [
		{
			"firstName" : {$regex: 'a', $options: `$i`}
		},
		{
			"isAdmin" : true
		}
	]
})

	db.users.find({
		$or: [
		{
			"lastName" : {$regex: 'w', $options: `$i`}
		},
		{
			"isAdmin" : false
		}
	]
})

// $and operator
	// allows us to have a logical operator wherein we can look or find documents which satisfy both of the conditions

	db.newProducts.find({
		$and: [
		{
			"name" : {$regex: 'razer', $options: `$i`}
		},
		{
			"price" : {$gte: 3000}
		}
	]
})

	db.users.find({
		$and: [
		{
			"lastName" : {$regex: 'w', $options: `$i`}
		},
		{
			"isAdmin" : false
		}
	]
})

	db.users.find({
		$and: [
		{
			"lastName" : {$regex: 'a', $options: `$i`}
		},
		{
			"firstName" : {$regex: 'e', $options: `$i`}
		}
	]
})

// Field Projection
	// allows us to hide/ show properties/ fields of the returned document after a query

	// in field projection, 0 means hide and 1 means show
	//db.collection.find({query}, {"field" : 0/1})
	
	db.users.find({}, {"_id": 0, "password": 0})

// find() can have two agruments
	//db.collection.find({query}, {projection})

	db.users.find({"isAdmin" : false}, {"_id" : 0, "email" : 1})

	db.newProducts.find(
		{
			"price" : {$gte: 10000},
		},
		{
			"_id" : 0, "name" : 1, "price" : 1
		}
	)
		